const showPass = document.getElementById("passwordShow");
const hidePass = document.getElementById("passwordHide");
const btn = document.getElementsByClassName("btn");
showPass.onclick = function () {

    const inputBtn1 = document.getElementById("inputBtn-1");
    showPass.classList.toggle("fa-eye");
    showPass.classList.toggle("fa-eye-slash");
    if (inputBtn1.hasAttribute('type')) {
        inputBtn1.removeAttribute("type")
    } else {
        inputBtn1.setAttribute('type', "password")
    }

};
hidePass.onclick = function () {
    const inputBtn2 = document.getElementById("inputBtn-2");
    hidePass.classList.toggle("fa-eye-slash");
    hidePass.classList.toggle("fa-eye");
    if (inputBtn2.hasAttribute('type')) {
        inputBtn2.removeAttribute("type")
    } else {
        inputBtn2.setAttribute('type', "password")
    }

};
btn[0].onclick = function () {
    const inputBtn1 = document.getElementById("inputBtn-1");
    const inputBtn2 = document.getElementById("inputBtn-2");
    if (inputBtn1.value === inputBtn2.value) {
        alert("Welcome")
    } else if (inputBtn2.value !== inputBtn1.value) {
        alert("Нужно ввести одинаковые значения")
    }
};
